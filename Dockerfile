FROM debian:latest
RUN apt update && apt install gcc g++ zsh -y
RUN chsh -s /usr/bin/zsh
ENTRYPOINT zsh
